package main

// Create a CLI to filter flowers from Flowers.csv by name, color, growing condition, region, season, description.
// Allow multiple filters and partial matches.
// Output matching rows as pretty printed JSON

import (
	"encoding/csv"
	"encoding/json"
	"flag"
	"fmt"
	"os"
	"strings"
)

type Flower struct {
	Name             string `json:"name"`
	Color            string `json:"color"`
	GrowingCondition string `json:"growingCondition"`
	Region           string `json:"region"`
	Season           string `json:"season"`
	Description      string `json:"description"`
}

func main() {
	name := flag.String("name", "", "filter by name")
	color := flag.String("color", "", "filter by color")
	condition := flag.String("condition", "", "filter by growing condition")
	region := flag.String("region", "", "filter by region")
	season := flag.String("season", "", "filter by season")
	desc := flag.String("desc", "", "filter by description")
	flag.Parse()

	f, err := os.Open("Flowers.csv")
	if err != nil {
		fmt.Println("Error opening file:", err)
		return
	}
	defer f.Close()

	r := csv.NewReader(f)
	rows, err := r.ReadAll()
	if err != nil {
		fmt.Println("Error reading CSV:", err)
		return
	}

	var flowers []Flower
	for _, row := range rows[1:] {
		flower := Flower{
			Name:             row[0],
			Color:            row[1],
			GrowingCondition: row[2],
			Region:           row[3],
			Season:           row[4],
			Description:      row[5],
		}

		if matchesFilters(flower, *name, *color, *condition, *region, *season, *desc) {
			flowers = append(flowers, flower)
		}
	}

	jsonBytes, err := json.MarshalIndent(flowers, "", "  ")
	if err != nil {
		fmt.Println("Error marshaling JSON:", err)
		return
	}

	fmt.Println(string(jsonBytes))
}

func matchesFilters(f Flower, name, color, condition, region, season, desc string) bool {
	if name != "" && !strings.Contains(strings.ToLower(f.Name), strings.ToLower(name)) {
		return false
	}
	if color != "" && !strings.Contains(strings.ToLower(f.Color), strings.ToLower(color)) {
		return false
	}
	if condition != "" && !strings.Contains(strings.ToLower(f.GrowingCondition), strings.ToLower(condition)) {
		return false
	}
	if region != "" && !strings.Contains(strings.ToLower(f.Region), strings.ToLower(region)) {
		return false
	}
	if season != "" && !strings.Contains(strings.ToLower(f.Season), strings.ToLower(season)) {
		return false
	}
	if desc != "" && !strings.Contains(strings.ToLower(f.Description), strings.ToLower(desc)) {
		return false
	}
	return true
}
